library code;

import 'dart:io';
import 'dart:math';

// PARTS
part 'entities/player.dart';
part 'entities/action.dart';
part 'entities/scenario.dart';

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/



void main() {
    List inputs;

    // game loop
    while (true) {

        List<Learn> recipes = [];
        List<Brew> orders = [];
        List<Cast> casts = [];
        List<Cast> opponentCasts = [];
        Me me;
        Opponent opponent;

        int actionCount = int.parse(stdin.readLineSync()); // the number of spells and recipes in play
        for (int i = 0; i < actionCount; i++) {
            inputs = stdin.readLineSync().split(' ');
            int actionId = int.parse(inputs[0]); // the unique ID of this spell or recipe
            String actionType = inputs[1]; // in the first league: BREW; later: CAST, OPPONENT_CAST, LEARN, BREW
            int delta0 = int.parse(inputs[2]); // tier-0 ingredient change
            int delta1 = int.parse(inputs[3]); // tier-1 ingredient change
            int delta2 = int.parse(inputs[4]); // tier-2 ingredient change
            int delta3 = int.parse(inputs[5]); // tier-3 ingredient change
            int price = int.parse(inputs[6]); // the price in rupees if this is a potion
            int tomeIndex = int.parse(inputs[7]); // in the first two leagues: always 0; later: the index in the tome if this is a tome spell, equal to the read-ahead tax
            int taxCount = int.parse(inputs[8]); // in the first two leagues: always 0; later: the amount of taxed tier-0 ingredients you gain from learning this spell
            int castable = int.parse(inputs[9]); // in the first league: always 0; later: 1 if this is a castable player spell
            int repeatable = int.parse(inputs[10]); // for the first two leagues: always 0; later: 1 if this is a repeatable player spell
        
            switch(actionType) {
                case "LEARN": {
                    recipes.add(Learn(
                        actionId,
                        actionType,
                        delta0,
                        delta1,
                        delta2,
                        delta3,
                        price,
                        tomeIndex,
                        taxCount,
                        castable,
                        repeatable,
                    ));
                }
                break;

                case "BREW": {
                    orders.add(Brew(
                        actionId,
                        actionType,
                        delta0,
                        delta1,
                        delta2,
                        delta3,
                        price,
                        tomeIndex,
                        taxCount,
                        castable,
                        repeatable,
                    ));
                }
                break;

                case "CAST": {
                    casts.add(Cast(
                        actionId,
                        actionType,
                        delta0,
                        delta1,
                        delta2,
                        delta3,
                        price,
                        tomeIndex,
                        taxCount,
                        castable,
                        repeatable,
                    ));
                }
                break;

                case "OPPONENT_CAST": {
                    opponentCasts.add(Cast(
                        actionId,
                        actionType,
                        delta0,
                        delta1,
                        delta2,
                        delta3,
                        price,
                        tomeIndex,
                        taxCount,
                        castable,
                        repeatable,
                    ));
                }
                break;
            }
        }

        for (int i = 0; i < 2; i++) {
            inputs = stdin.readLineSync().split(' ');
            int inv0 = int.parse(inputs[0]); // tier-0 ingredients in inventory
            int inv1 = int.parse(inputs[1]);
            int inv2 = int.parse(inputs[2]);
            int inv3 = int.parse(inputs[3]);
            int score = int.parse(inputs[4]); // amount of rupees

            if (i == 0) me = Me(inv0, inv1, inv2, inv3, score, casts, orders, recipes);
            else opponent = Opponent(inv0, inv1, inv2, inv3, score, opponentCasts);
        }

        print(me.nextAction());
    }
}