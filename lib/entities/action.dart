part of code;

abstract class Action {
    int actionId;
    String actionType;
    int delta0;
    int delta1;
    int delta2;
    int delta3;
    int price;
    int tomeIndex;
    int taxCount;
    int castable;
    int repeatable;

    Action(
        this.actionId,
        this.actionType,
        this.delta0,
        this.delta1,
        this.delta2,
        this.delta3,
        this.price,
        this.tomeIndex,
        this.taxCount,
        this.castable,
        this.repeatable,
    );

    Action.rest();

    String get command;
}

class Cast extends Action {
  int times;

    Cast(
        int actionId,
        String actionType,
        int delta0,
        int delta1,
        int delta2,
        int delta3,
        int price,
        int tomeIndex,
        int taxCount,
        int castable,
        int repeatable,
        [
          this.times = 1,
        ]
    ) : super(actionId, actionType, delta0, delta1, delta2, delta3, price, tomeIndex, taxCount, castable, repeatable);

    @override
    String get command => 'CAST $actionId $times';

    Cast exhausted() {
      castable = 0;
      return this;
    }

    Cast available() {
      castable = 1;
      return this;
    }
}

class Learn extends Action {
    Learn(
        int actionId,
        String actionType,
        int delta0,
        int delta1,
        int delta2,
        int delta3,
        int price,
        int tomeIndex,
        int taxCount,
        int castable,
        int repeatable,
    ) : super(actionId, actionType, delta0, delta1, delta2, delta3, price, tomeIndex, taxCount, castable, repeatable);

    @override
    String get command => 'LEARN $actionId';
}

class Brew extends Action {
    Brew(
        int actionId,
        String actionType,
        int delta0,
        int delta1,
        int delta2,
        int delta3,
        int price,
        int tomeIndex,
        int taxCount,
        int castable,
        int repeatable,
    ) : super(actionId, actionType, delta0, delta1, delta2, delta3, price, tomeIndex, taxCount, castable, repeatable);

    @override
    String get command => 'BREW $actionId';

    int get ingredientsToCraft => delta1 + delta2 + delta3;

    int get priceWithBonus => price + tomeIndex;
}

class Rest extends Action{
    Rest(): super.rest();

    @override
    String get command => 'REST';
}