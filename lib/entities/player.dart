part of code;

abstract class Player {

    int score;
    List<int> inventory;
    List<Cast> casts;
    List<Brew> orders;
    List<Learn> recipes;

    Player(
        this.inventory,
        this.score,
        this.casts,
        [
            this.orders,
            this.recipes,
        ]
    );

    int get inventorySpace => 10 - inventory.reduce((v, e) => v + e);
}

class Me extends Player {
    int futureIteration;
    List<Scenario> scenarios = [];

    Me(
        int inv0, 
        int inv1, 
        int inv2, 
        int inv3, 
        int score,
        List<Cast> casts,
        List<Brew> orders,
        List<Learn> recipes,
    ) : super([inv0, inv1, inv2, inv3], score, casts, orders, recipes);

    Me.afterBrew(
      Me snapshot,
      Brew brewed,
    ) : this(snapshot.inventory[0] + brewed.delta0, snapshot.inventory[1] + brewed.delta1, snapshot.inventory[2] + brewed.delta2, snapshot.inventory[3] + brewed.delta3, snapshot.score + brewed.priceWithBonus, snapshot.casts, snapshot.orders.where((order) => order.actionId != brewed.actionId).toList(), snapshot.recipes);

    Me.afterLearn(
      Me snapshot,
      Learn learned,
    ) : this(snapshot.inventory[0] - learned.tomeIndex, snapshot.inventory[1], snapshot.inventory[2], snapshot.inventory[3], snapshot.score, snapshot.casts, snapshot.orders, snapshot.recipes.where((recipe) => recipe.actionId != learned.actionId).toList());

    Me.afterCast(
      Me snapshot,
      Cast casted,
    ) : this(snapshot.inventory[0] + casted.delta0, snapshot.inventory[1] + casted.delta1, snapshot.inventory[2] + casted.delta2, snapshot.inventory[3] + casted.delta3, snapshot.score, snapshot.casts.map((cast) => cast.actionId == casted.actionId ? cast.exhausted() : cast).toList(), snapshot.orders, snapshot.recipes);

    Me.afterRest(
      Me snapshot,
    ) : this(snapshot.inventory[0], snapshot.inventory[1], snapshot.inventory[2], snapshot.inventory[3], snapshot.score, snapshot.casts.map((cast) => cast.available()).toList(), snapshot.orders, snapshot.recipes);

    String nextAction() {
      generateScenarios();
      return bestScenario(scenarios).command;
    }

    Scenario bestScenario(List<Scenario> scenarios) {
      scenarios.sort((a, b) => b.value.compareTo(a.value));
      return scenarios.first;
    }

    void generateScenarios() {
      List<Action> actions = getAllPossibleActions(this);

      actions.forEach((action) {
        Me firstSnapshot = generateSnapshot(action, this);
        List<Action> actions2 = getAllPossibleActions(firstSnapshot);

        actions2.forEach((action2) {
          Me secondSnapshot = generateSnapshot(action2, firstSnapshot);
          List<Action> actions3 = getAllPossibleActions(secondSnapshot);

          actions3.forEach((action3) {
            Me thirdSnapshot = generateSnapshot(action3, secondSnapshot);
            Scenario scenario = Scenario([action.command, action2.command, action3.command]);
            scenario.evaluateScenario(thirdSnapshot);
            scenarios.add(scenario);
          });
        });
      });
    }

    Me generateSnapshot(Action action, Me snapshot) {
      if (action.actionType == 'BREW') return Me.afterBrew(snapshot, action);
      if (action.actionType == 'LEARN') return Me.afterLearn(snapshot, action);
      if (action.actionType == 'CAST') return Me.afterCast(snapshot, action);
      return Me.afterRest(snapshot);
    }

    List<Action> getAllPossibleActions(Me snapshot) {
      List<Action> actions = [];
      actions.addAll(getAllPossibleBrews(snapshot));
      actions.addAll(getAllPossibleLearns(snapshot));
      actions.addAll(getAllPossibleCasts(snapshot));
      actions.add(Rest());
      return actions;
    }

    List<Brew> getAllPossibleBrews(Me snapshot) {
      bool haveEnoughElements(Brew brew, List<int> inventory) =>  snapshot.inventory[0] + brew.delta0 >= 0 && snapshot.inventory[1] + brew.delta1 >= 0 && snapshot.inventory[2] + brew.delta2 >= 0 && snapshot.inventory[3] + brew.delta3 >= 0;
      List<Brew> possibleBrews = snapshot.orders.where((order) => haveEnoughElements(order, snapshot.inventory)).toList();
      return possibleBrews;
    }

    List<Learn> getAllPossibleLearns(Me snapshot) {
      List<Learn> possibleRecipes = snapshot.recipes.where((recipe) => recipe.tomeIndex <= snapshot.inventory[0]).toList();
      return possibleRecipes;
    }

    List<Cast> getAllPossibleCasts(Me snapshot) {
      bool isCastable(Cast cast) => cast.castable == 1;
      bool haveEnoughElements(Cast cast, List<int> inventory) =>  snapshot.inventory[0] + cast.delta0 >= 0 && snapshot.inventory[1] + cast.delta1 >= 0 && snapshot.inventory[2] + cast.delta2 >= 0 && snapshot.inventory[3] + cast.delta3 >= 0;
      bool haveEnoughSpace(Cast cast, int availableSpace) => (cast.delta0 + cast.delta1 + cast.delta2 + cast.delta3) <= availableSpace;
      List<Cast> possibleCasts = snapshot.casts.where((cast) => isCastable(cast) && haveEnoughElements(cast, snapshot.inventory) && haveEnoughSpace(cast, snapshot.inventorySpace)).toList();
      return possibleCasts;
    }

}

class Opponent extends Player {
    Opponent(
        int inv0, 
        int inv1, 
        int inv2, 
        int inv3, 
        int score,
        List<Cast> casts,
    ) : super([inv0, inv1, inv2, inv3], score, casts);
}