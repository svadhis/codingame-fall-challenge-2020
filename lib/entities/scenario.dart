part of code;

class Scenario {
  List<String> actions;
  double value;

  Scenario(this.actions);

  String get command => actions[0];

  void evaluateScenario(Me snapshot) {
    double result = 0;

    for (var i = 0; i < actions.length; i++) {
      String action = actions[i];

      // Give bonus points points for action (brew: 30 25 20, cast: 10 8 6, learn: 8 6 4)
      if (action.startsWith('BREW')) result += 100 - (i * 10);
      if (action.startsWith('CAST')) result += snapshot.casts.length < 10 ? 0 : 10 - (i * 2);
      if (action.startsWith('LEARN')) result += 8 - (i * 2);

      // Give points for total elements in inventory (2 for 0, 3 for 1, 4 for 2, 5 for 3)
      for (var j = 0; j < snapshot.inventory.length; j++) {
        result += (snapshot.inventory[j] * 2) + (snapshot.inventory[j] / 2 * j);
      }

      // Give points for castable casts (0.5 for castable, 0 for exhausted + 0.5 for repeatable)
      result += snapshot.casts.map((cast) => cast.castable / 2 + cast.repeatable / 2).reduce((v, e) => v + e);
    }

    value = result;
  }
}