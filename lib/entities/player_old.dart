part of code;

abstract class Player {

    int inv0;
    int inv1;
    int inv2;
    int inv3;
    int score;
    List<Action> casts;
    List<Brew> orders;
    List<Learn> recipes;

    Player(
        this.inv0,
        this.inv1,
        this.inv2,
        this.inv3,
        this.score,
        this.casts,
        [
            this.orders,
            this.recipes,
        ]
    );

    int get inventorySpace => 10 - inv0 - inv1 - inv2 - inv3;

    Cast get cast0 => casts[0];
    Cast get cast1 => casts[1];
    Cast get cast2 => casts[2];
    Cast get cast3 => casts[3];
}

class Me extends Player {
    Brew nextOrder;

    Map<List<String>, int> brewScenarios = {};
    Map<List<String>, List<int>> scenarios = {};

    Me(
        int inv0, 
        int inv1, 
        int inv2, 
        int inv3, 
        int score,
        List<Cast> casts,
        List<Brew> orders,
        List<Learn> recipes,
    ) : super(inv0, inv1, inv2, inv3, score, casts, orders, recipes) {
        sortOrders();
        nextOrder = orders[0];
    }

    bool get needIngredient3 => inv3 + nextOrder.delta3 < 0;
    bool get needIngredient2 => inv2 + nextOrder.delta2 < 0 || needIngredient3;
    bool get needIngredient1 => inv1 + nextOrder.delta1 < 0 || needIngredient2;
    bool get needIngredient0 => inv0 + nextOrder.delta0 < 0 || needIngredient1;

    bool get haveIngredient3 => inv3 > 0;
    bool get haveIngredient2 => inv2 > 0;
    bool get haveIngredient1 => inv1 > 0;
    bool get haveIngredient0 => inv0 > 0;

    bool get canCastSpell3 => cast3.castable == 1;
    bool get canCastSpell2 => cast2.castable == 1;
    bool get canCastSpell1 => cast1.castable == 1;
    bool get canCastSpell0 => cast0.castable == 1;

    void sortOrders() => orders.sort((a, b) {
        int sortResult = craftingValue(b).compareTo(craftingValue(a));
        // If equals, get the one that gives more money
        if (sortResult != 0) return sortResult;
        return b.priceWithBonus.compareTo(a.priceWithBonus);
    });

    int craftingValue(Brew brew) {
      double ingredient2Malus = 1.3;
      double ingredient3Malus = 1.6;
      double stepsToCraftBrew = (brew.delta1 - inv1) + ((brew.delta2 - inv2) * ingredient2Malus) + ((brew.delta3 - inv3) * ingredient3Malus);

      double priceQuotient = 0.3;

      return stepsToCraftBrew ~/ (brew.priceWithBonus * priceQuotient);
    }

    bool get haveAllIngredientsForAnyBrew {
        for (int i = 0; i < orders.length; i++) {
            if (inv0 + orders[i].delta0 >= 0 && inv1 + orders[i].delta1 >= 0 && inv2 + orders[i].delta2 >= 0 && inv3 + orders[i].delta3 >= 0) {
                nextOrder = orders[i];
                return true;
            }
        }
        return false;
    } 
    
    String nextAction() {
        // If I can prepare ANY brew, prepare it
        if (haveAllIngredientsForAnyBrew) return 'BREW ${nextOrder.actionId}';

        // Else, check for every spell if I can cast it
        if (needIngredient3 && canCastSpell3 && haveIngredient2) return 'CAST ${cast3.actionId}';

        if (needIngredient2 && canCastSpell2 && haveIngredient1) return 'CAST ${cast2.actionId}';

        if (needIngredient1 && canCastSpell1 && haveIngredient0) return 'CAST ${cast1.actionId}';

        if (needIngredient0 && canCastSpell0 && inventorySpace >= 3) return 'CAST ${cast0.actionId} $needIngredient0 $needIngredient1 $needIngredient2 $needIngredient3';

        // Else, rest
        return 'REST';
    }
}

class Opponent extends Player {
    Opponent(
        int inv0, 
        int inv1, 
        int inv2, 
        int inv3, 
        int score,
        List<Cast> casts,
    ) : super(inv0, inv1, inv2, inv3, score, casts);
}