library code;

import 'dart:io';
import 'dart:math';

// PARTS
// PART FILE entities/player.dart

abstract class Player {

    int score;
    List<int> inventory;
    List<Cast> casts;
    List<Brew> orders;
    List<Learn> recipes;

    Player(
        this.inventory,
        this.score,
        this.casts,
        [
            this.orders,
            this.recipes,
        ]
    );

    int get inventorySpace => 10 - inventory.reduce((v, e) => v + e);
}

class Me extends Player {
    int futureIteration;
    List<Scenario> scenarios = [];

    Me(
        int inv0, 
        int inv1, 
        int inv2, 
        int inv3, 
        int score,
        List<Cast> casts,
        List<Brew> orders,
        List<Learn> recipes,
    ) : super([inv0, inv1, inv2, inv3], score, casts, orders, recipes);

    Me.afterBrew(
      Me snapshot,
      Brew brewed,
    ) : this(snapshot.inventory[0] + brewed.delta0, snapshot.inventory[1] + brewed.delta1, snapshot.inventory[2] + brewed.delta2, snapshot.inventory[3] + brewed.delta3, snapshot.score + brewed.priceWithBonus, snapshot.casts, snapshot.orders.where((order) => order.actionId != brewed.actionId).toList(), snapshot.recipes);

    Me.afterLearn(
      Me snapshot,
      Learn learned,
    ) : this(snapshot.inventory[0] - learned.tomeIndex, snapshot.inventory[1], snapshot.inventory[2], snapshot.inventory[3], snapshot.score, snapshot.casts, snapshot.orders, snapshot.recipes.where((recipe) => recipe.actionId != learned.actionId).toList());

    Me.afterCast(
      Me snapshot,
      Cast casted,
    ) : this(snapshot.inventory[0] + casted.delta0, snapshot.inventory[1] + casted.delta1, snapshot.inventory[2] + casted.delta2, snapshot.inventory[3] + casted.delta3, snapshot.score, snapshot.casts.map((cast) => cast.actionId == casted.actionId ? cast.exhausted() : cast).toList(), snapshot.orders, snapshot.recipes);

    Me.afterRest(
      Me snapshot,
    ) : this(snapshot.inventory[0], snapshot.inventory[1], snapshot.inventory[2], snapshot.inventory[3], snapshot.score, snapshot.casts.map((cast) => cast.available()).toList(), snapshot.orders, snapshot.recipes);

    String nextAction() {
      generateScenarios();
      return bestScenario(scenarios).command;
    }

    Scenario bestScenario(List<Scenario> scenarios) {
      scenarios.sort((a, b) => b.value.compareTo(a.value));
      return scenarios.first;
    }

    void generateScenarios() {
      List<Action> actions = getAllPossibleActions(this);

      actions.forEach((action) {
        Me firstSnapshot = generateSnapshot(action, this);
        List<Action> actions2 = getAllPossibleActions(firstSnapshot);

        actions2.forEach((action2) {
          Me secondSnapshot = generateSnapshot(action2, firstSnapshot);
          List<Action> actions3 = getAllPossibleActions(secondSnapshot);

          actions3.forEach((action3) {
            Me thirdSnapshot = generateSnapshot(action3, secondSnapshot);
            Scenario scenario = Scenario([action.command, action2.command, action3.command]);
            scenario.evaluateScenario(thirdSnapshot);
            scenarios.add(scenario);
          });
        });
      });
    }

    Me generateSnapshot(Action action, Me snapshot) {
      if (action.actionType == 'BREW') return Me.afterBrew(snapshot, action);
      if (action.actionType == 'LEARN') return Me.afterLearn(snapshot, action);
      if (action.actionType == 'CAST') return Me.afterCast(snapshot, action);
      return Me.afterRest(snapshot);
    }

    List<Action> getAllPossibleActions(Me snapshot) {
      List<Action> actions = [];
      actions.addAll(getAllPossibleBrews(snapshot));
      actions.addAll(getAllPossibleLearns(snapshot));
      actions.addAll(getAllPossibleCasts(snapshot));
      actions.add(Rest());
      return actions;
    }

    List<Brew> getAllPossibleBrews(Me snapshot) {
      bool haveEnoughElements(Brew brew, List<int> inventory) =>  snapshot.inventory[0] + brew.delta0 >= 0 && snapshot.inventory[1] + brew.delta1 >= 0 && snapshot.inventory[2] + brew.delta2 >= 0 && snapshot.inventory[3] + brew.delta3 >= 0;
      List<Brew> possibleBrews = snapshot.orders.where((order) => haveEnoughElements(order, snapshot.inventory)).toList();
      return possibleBrews;
    }

    List<Learn> getAllPossibleLearns(Me snapshot) {
      List<Learn> possibleRecipes = snapshot.recipes.where((recipe) => recipe.tomeIndex <= snapshot.inventory[0]).toList();
      return possibleRecipes;
    }

    List<Cast> getAllPossibleCasts(Me snapshot) {
      bool isCastable(Cast cast) => cast.castable == 1;
      bool haveEnoughElements(Cast cast, List<int> inventory) =>  snapshot.inventory[0] + cast.delta0 >= 0 && snapshot.inventory[1] + cast.delta1 >= 0 && snapshot.inventory[2] + cast.delta2 >= 0 && snapshot.inventory[3] + cast.delta3 >= 0;
      bool haveEnoughSpace(Cast cast, int availableSpace) => (cast.delta0 + cast.delta1 + cast.delta2 + cast.delta3) <= availableSpace;
      List<Cast> possibleCasts = snapshot.casts.where((cast) => isCastable(cast) && haveEnoughElements(cast, snapshot.inventory) && haveEnoughSpace(cast, snapshot.inventorySpace)).toList();
      return possibleCasts;
    }

}

class Opponent extends Player {
    Opponent(
        int inv0, 
        int inv1, 
        int inv2, 
        int inv3, 
        int score,
        List<Cast> casts,
    ) : super([inv0, inv1, inv2, inv3], score, casts);
}
// PART FILE entities/action.dart

abstract class Action {
    int actionId;
    String actionType;
    int delta0;
    int delta1;
    int delta2;
    int delta3;
    int price;
    int tomeIndex;
    int taxCount;
    int castable;
    int repeatable;

    Action(
        this.actionId,
        this.actionType,
        this.delta0,
        this.delta1,
        this.delta2,
        this.delta3,
        this.price,
        this.tomeIndex,
        this.taxCount,
        this.castable,
        this.repeatable,
    );

    Action.rest();

    String get command;
}

class Cast extends Action {
  int times;

    Cast(
        int actionId,
        String actionType,
        int delta0,
        int delta1,
        int delta2,
        int delta3,
        int price,
        int tomeIndex,
        int taxCount,
        int castable,
        int repeatable,
        [
          this.times = 1,
        ]
    ) : super(actionId, actionType, delta0, delta1, delta2, delta3, price, tomeIndex, taxCount, castable, repeatable);

    @override
    String get command => 'CAST $actionId $times';

    Cast exhausted() {
      castable = 0;
      return this;
    }

    Cast available() {
      castable = 1;
      return this;
    }
}

class Learn extends Action {
    Learn(
        int actionId,
        String actionType,
        int delta0,
        int delta1,
        int delta2,
        int delta3,
        int price,
        int tomeIndex,
        int taxCount,
        int castable,
        int repeatable,
    ) : super(actionId, actionType, delta0, delta1, delta2, delta3, price, tomeIndex, taxCount, castable, repeatable);

    @override
    String get command => 'LEARN $actionId';
}

class Brew extends Action {
    Brew(
        int actionId,
        String actionType,
        int delta0,
        int delta1,
        int delta2,
        int delta3,
        int price,
        int tomeIndex,
        int taxCount,
        int castable,
        int repeatable,
    ) : super(actionId, actionType, delta0, delta1, delta2, delta3, price, tomeIndex, taxCount, castable, repeatable);

    @override
    String get command => 'BREW $actionId';

    int get ingredientsToCraft => delta1 + delta2 + delta3;

    int get priceWithBonus => price + tomeIndex;
}

class Rest extends Action{
    Rest(): super.rest();

    @override
    String get command => 'REST';
}
// PART FILE entities/scenario.dart

class Scenario {
  List<String> actions;
  double value;

  Scenario(this.actions);

  String get command => actions[0];

  void evaluateScenario(Me snapshot) {
    double result = 0;

    for (var i = 0; i < actions.length; i++) {
      String action = actions[i];

      // Give bonus points points for action (brew: 30 25 20, cast: 10 8 6, learn: 8 6 4)
      if (action.startsWith('BREW')) result += 100 - (i * 10);
      if (action.startsWith('CAST')) result += snapshot.casts.length < 10 ? 0 : 10 - (i * 2);
      if (action.startsWith('LEARN')) result += 8 - (i * 2);

      // Give points for total elements in inventory (2 for 0, 3 for 1, 4 for 2, 5 for 3)
      for (var j = 0; j < snapshot.inventory.length; j++) {
        result += (snapshot.inventory[j] * 2) + (snapshot.inventory[j] / 2 * j);
      }

      // Give points for castable casts (0.5 for castable, 0 for exhausted + 0.5 for repeatable)
      result += snapshot.casts.map((cast) => cast.castable / 2 + cast.repeatable / 2).reduce((v, e) => v + e);
    }

    value = result;
  }
}

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/



void main() {
    List inputs;

    // game loop
    while (true) {

        List<Learn> recipes = [];
        List<Brew> orders = [];
        List<Cast> casts = [];
        List<Cast> opponentCasts = [];
        Me me;
        Opponent opponent;

        int actionCount = int.parse(stdin.readLineSync()); // the number of spells and recipes in play
        for (int i = 0; i < actionCount; i++) {
            inputs = stdin.readLineSync().split(' ');
            int actionId = int.parse(inputs[0]); // the unique ID of this spell or recipe
            String actionType = inputs[1]; // in the first league: BREW; later: CAST, OPPONENT_CAST, LEARN, BREW
            int delta0 = int.parse(inputs[2]); // tier-0 ingredient change
            int delta1 = int.parse(inputs[3]); // tier-1 ingredient change
            int delta2 = int.parse(inputs[4]); // tier-2 ingredient change
            int delta3 = int.parse(inputs[5]); // tier-3 ingredient change
            int price = int.parse(inputs[6]); // the price in rupees if this is a potion
            int tomeIndex = int.parse(inputs[7]); // in the first two leagues: always 0; later: the index in the tome if this is a tome spell, equal to the read-ahead tax
            int taxCount = int.parse(inputs[8]); // in the first two leagues: always 0; later: the amount of taxed tier-0 ingredients you gain from learning this spell
            int castable = int.parse(inputs[9]); // in the first league: always 0; later: 1 if this is a castable player spell
            int repeatable = int.parse(inputs[10]); // for the first two leagues: always 0; later: 1 if this is a repeatable player spell
        
            switch(actionType) {
                case "LEARN": {
                    recipes.add(Learn(
                        actionId,
                        actionType,
                        delta0,
                        delta1,
                        delta2,
                        delta3,
                        price,
                        tomeIndex,
                        taxCount,
                        castable,
                        repeatable,
                    ));
                }
                break;

                case "BREW": {
                    orders.add(Brew(
                        actionId,
                        actionType,
                        delta0,
                        delta1,
                        delta2,
                        delta3,
                        price,
                        tomeIndex,
                        taxCount,
                        castable,
                        repeatable,
                    ));
                }
                break;

                case "CAST": {
                    casts.add(Cast(
                        actionId,
                        actionType,
                        delta0,
                        delta1,
                        delta2,
                        delta3,
                        price,
                        tomeIndex,
                        taxCount,
                        castable,
                        repeatable,
                    ));
                }
                break;

                case "OPPONENT_CAST": {
                    opponentCasts.add(Cast(
                        actionId,
                        actionType,
                        delta0,
                        delta1,
                        delta2,
                        delta3,
                        price,
                        tomeIndex,
                        taxCount,
                        castable,
                        repeatable,
                    ));
                }
                break;
            }
        }

        for (int i = 0; i < 2; i++) {
            inputs = stdin.readLineSync().split(' ');
            int inv0 = int.parse(inputs[0]); // tier-0 ingredients in inventory
            int inv1 = int.parse(inputs[1]);
            int inv2 = int.parse(inputs[2]);
            int inv3 = int.parse(inputs[3]);
            int score = int.parse(inputs[4]); // amount of rupees

            if (i == 0) me = Me(inv0, inv1, inv2, inv3, score, casts, orders, recipes);
            else opponent = Opponent(inv0, inv1, inv2, inv3, score, opponentCasts);
        }

        print(me.nextAction());
    }
}